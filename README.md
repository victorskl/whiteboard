# Whiteboard

Whiteboard is a Enterprise Learning Management System

## Demo
https://cloud.sankholin.com/whiteboard/

## Team 36

Members:

- San Kho Lin sanl1@student.unimelb.edu.au
- Ruijing Zhang ruijingz@student.unimelb.edu.au

SWEN90007 Software Design and Architecture SM2 2017, The University of Melbourne